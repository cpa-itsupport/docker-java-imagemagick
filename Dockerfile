FROM openjdk:8-alpine
ENV SCRIPT_URL=''
ENV APP_OPTS=''
ENV JAVA_OPTS=''
ENV APP_URL=''
RUN apk add -U imagemagick bash && \
    mkdir -p /app/data/pdf && \
    mkdir -p /app/data/png
VOLUME /app
WORKDIR /app
ADD run.sh /run.sh
CMD ["/run.sh"]
