#!/bin/sh
PRIVATE_SCRIPT=/app/private.sh
APP_JAR=/app/app.jar
# this is a new image
if [ -n "$PRIVATE_SCRIPT" ]; then
  wget -O $PRIVATE_SCRIPT $SCRIPT_URL && wget -O $APP_JAR $APP_URL && chmod +x $PRIVATE_SCRIPT
fi
$PRIVATE_SCRIPT
